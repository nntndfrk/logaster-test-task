import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LogoPageComponent } from './logo-page/logo-page.component';
import { FontsService } from './core/services/fonts.service';
import { environment } from 'src/environments/environment';

const routes: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'main', component: MainComponent },
    { path: 'logo/:id', component: LogoPageComponent },
    { path: 'create', component: LogoPageComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
