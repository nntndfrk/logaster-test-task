import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { LogoConfigModel } from '../core/models/logo-config.model';
import { MainService } from '../core/services/main.service';
import { FontsService } from '../core/services/fonts.service';
import { DeleteConfirmService } from '../core/services/delete-confirm.service';

@Component({
    selector: 'app-logo-page',
    templateUrl: './logo-page.component.html',
    styleUrls: ['./logo-page.component.css']
})
export class LogoPageComponent implements OnInit {
    logoId: string;
    config: LogoConfigModel;
    isEdit = false;
    activeUrl = '';

    constructor(
        private fontService: FontsService,
        private activateRoute: ActivatedRoute,
        private router: Router,
        private mainService: MainService,
        private deleteConfirmService: DeleteConfirmService
    ) {}

    ngOnInit() {
        this.activeUrl = this.activateRoute.snapshot.url.join().split(',')[0];

        if (this.activeUrl === 'create') {
            this.initTemplatePage();
        } else {
            this.initLogoPage();
        }
    }

    async updateLogo(config: LogoConfigModel) {
        await this.fontService.chacheFont(config.fontFamily);
        this.config = config;
    }

    initTemplatePage() {
        this.isEdit = true;
        this.mainService.getTemplateLogo().subscribe(config => {
            this.config = config;
        });
    }

    initLogoPage() {
        this.logoId = this.activateRoute.snapshot.params.id;
        this.isEdit = this.activateRoute.snapshot.queryParams.edit;

        combineLatest(this.activateRoute.params, this.activateRoute.queryParams)
            .pipe(
                map(results => ({
                    id: results[0].id,
                    isEdit: results[1].edit
                })),
                switchMap((routerParams: any) => {
                    this.isEdit = routerParams.isEdit;
                    return this.mainService.getLogoById(routerParams.id);
                })
            )
            .subscribe((config: LogoConfigModel) => {
                this.config = config;
            });

        this.mainService.getLogoById(this.logoId).subscribe(config => {
            this.config = config;
        });
    }

    edit() {
        this.isEdit = true;
        this.router.navigate(['logo', this.logoId], {
            queryParams: { edit: true }
        });
    }

    cancel() {
        if (this.activeUrl === 'create') {
            this.router.navigate(['main']);
        } else {
            this.isEdit = false;
            this.router.navigate(['logo', this.logoId]);
        }
    }

    delete() {
        this.deleteConfirmService.show().subscribe((status: boolean) => {
            if (status) {
                this.mainService.deleteLogo(this.logoId).subscribe(() => {
                    this.router.navigate(['main']);
                });
            }
        });
    }

    backToHome() {
        this.router.navigate(['main']);
    }

    save() {
        if (this.activeUrl === 'create') {
            this.mainService.saveLogo(this.config).subscribe(() => {
                this.router.navigate(['main']);
            });
        } else {
            this.isEdit = false;
            this.mainService
                .updateLogo(this.logoId, this.config)
                .subscribe(() => {
                    this.router.navigate(['logo', this.logoId]);
                });
        }
    }
}
