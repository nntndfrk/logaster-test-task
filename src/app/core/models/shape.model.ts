export type ShapeModel = 'square' | 'triangle' | 'circle';

export const SHAPES = ['square', 'triangle', 'circle'];
