import { ShapeModel } from './shape.model';

export class LogoConfigModel {
    id?: number;
    size?: number;
    name: string;
    shape: ShapeModel;
    slogan: string;
    shapeFillColor: string;
    textColor: string;
    fontFamily: string;
}
