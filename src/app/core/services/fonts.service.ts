import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, tap, pluck } from 'rxjs/operators';
import { FontModel } from '../models/font.model';

declare let FontFace: any;
@Injectable()
export class FontsService {
    sortOption = 'popularity';
    fontsList: FontModel[] = [];
    chachedFontsList = [];
    constructor(private http: HttpClient) {
    }

    getFonts(): Observable<FontModel[]> {
        if (!this.fontsList.length) {
            return this.http
                .get(`${environment.fontsUrl}?sort=${this.sortOption}&key=${environment.googleFontsApiKey}`)
                .pipe(
                    pluck('items'),
                    map((fonts: any[]) => {
                        return fonts.map(font => {
                            return {
                                family: font.family,
                                url: font.files.regular
                            };
                        });
                    }),
                    tap((fonts: FontModel[]) => {
                        this.fontsList = fonts;
                    })
                );
        } else {
            return of(this.fontsList);
        }
    }

    chacheFonts(logosList: any[]): Observable<any> {
        const loadTasksQueue = [];
        const doneBus = new Subject<any>();
        const currentChachedFontsList = [];

        logosList.forEach((logoElement: any) => {
            const font = this.fontsList.find(curFont => {
                return curFont.family === logoElement.fontFamily;
            });

            if (!this.chachedFontsList.includes(font.family)) {
                currentChachedFontsList.push(font.family);
                const fontFace = new FontFace(font.family, `url(${font.url})`);
                loadTasksQueue.push(fontFace.load());
            }

        });

        if (loadTasksQueue.length !== 0) {
            Promise.all(loadTasksQueue).then((loadTaskResults: any[]) => {
                loadTaskResults.forEach(res => {
                    document['fonts'].add(res);
                });

                doneBus.next(logosList);

                this.chachedFontsList = [
                    ...this.chachedFontsList,
                    ...currentChachedFontsList
                ];
            });
        } else {
            Promise.resolve().then(() => {
                doneBus.next(logosList);
            });
        }

        return doneBus.asObservable();
    }

    chacheFont(fontName: string): Promise<any> {

        const font = this.fontsList.find(curFont => {
            return curFont.family === fontName;
        });

        if (!this.chachedFontsList.includes(fontName)) {
            const fontFace = new FontFace(font.family, `url(${font.url})`);

            fontFace.load().then((res) => {
                document['fonts'].add(res);

                this.chachedFontsList = [...this.chachedFontsList, fontName];
            });

            return fontFace.loaded;
        } else {
            Promise.resolve();
        }
    }
}
