import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { take, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DeleteConfirmService {
    isDisplayBus$ = new BehaviorSubject<boolean>(false);
    dataBus$ = new Subject<boolean>();
    constructor() {}

    getDisplayBus() {
        return this.isDisplayBus$.asObservable();
    }

    getData() {
        return this.dataBus$.asObservable();
    }

    pubData(status: boolean) {
        this.dataBus$.next(status);
    }

    show() {
        this.isDisplayBus$.next(true);
        return this.getData().pipe(
            take(1),
            tap(() => {
                this.hide();
            })
        );
    }

    hide() {
        this.isDisplayBus$.next(false);
    }
}
