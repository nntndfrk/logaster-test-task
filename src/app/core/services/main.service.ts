import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap, switchMap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { LogoConfigModel } from '../models/logo-config.model';
import { FontsService } from './fonts.service';

@Injectable({
    providedIn: 'root'
})
export class MainService {
    logos: LogoConfigModel[];

    templateConfig: LogoConfigModel = {
        name: 'Template',
        shape: 'square',
        slogan: 'template',
        shapeFillColor: '#000',
        textColor: '#000',
        fontFamily: 'Roboto'
    };

    constructor(private http: HttpClient, private fontService: FontsService) {}

    getAllLogos(): Observable<any> {
        return this.fontService.getFonts().pipe(
            switchMap(fontList => {
                return this.http.get<LogoConfigModel[]>(`${environment.apiUrl}`);
            }),
            tap((logos: LogoConfigModel[]) => {
                this.logos = logos;
            }),
            map(logos => logos.map(logo => logo.fontFamily)),
            switchMap((_: any) => {
                return this.fontService.chacheFonts(this.logos);
            })
        );
    }

    getLogoById(id: string): Observable<LogoConfigModel> {
        return this.http.get<LogoConfigModel>(`${environment.apiUrl}/${id}`);
    }

    getTemplateLogo(): Observable<LogoConfigModel> {
        return of(this.templateConfig);
    }

    updateLogo(id: string, payload: LogoConfigModel) {
        return this.http.put(`${environment.apiUrl}/${id}`, payload);
    }

    saveLogo(payload: LogoConfigModel) {
        return this.http.post(`${environment.apiUrl}`, payload);
    }

    deleteLogo(id: string) {
        return this.http.delete(`${environment.apiUrl}/${id}`);
    }
}
