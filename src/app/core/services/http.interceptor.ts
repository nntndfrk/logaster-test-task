import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

import { ProgressService } from './progress.service';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
    constructor(private progressService: ProgressService) {}

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        this.progressService.setLoadState(true);

        return next.handle(req).pipe(
            delay(1000),
            tap(() => {
                this.progressService.setLoadState(false);
            })
        );
    }
}
