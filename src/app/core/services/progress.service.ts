import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProgressService {
    isLoadState = new BehaviorSubject<boolean>(false);
    constructor() {}

    getLoadState() {
        return this.isLoadState.asObservable();
    }

    setLoadState(state: boolean) {
        this.isLoadState.next(state);
    }
}
