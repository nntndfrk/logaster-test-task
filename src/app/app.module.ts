import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MainComponent } from './main/main.component';
import { LogoPageComponent } from './logo-page/logo-page.component';
import { AppInterceptor } from './core/services/http.interceptor';
import { FontsService } from './core/services/fonts.service';

@NgModule({
    declarations: [AppComponent, MainComponent, LogoPageComponent],
    imports: [BrowserModule, AppRoutingModule, SharedModule, HttpClientModule],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
        FontsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
