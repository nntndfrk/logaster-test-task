import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { MainService } from '../core/services/main.service';
import { LogoConfigModel } from '../core/models/logo-config.model';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
    logosConfigsList: Observable<LogoConfigModel[]>;

    constructor(private service: MainService) {}

    ngOnInit() {
        this.logosConfigsList = this.service.getAllLogos();
    }
}
