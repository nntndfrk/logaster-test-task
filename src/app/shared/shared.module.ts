import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KonvaModule } from 'ng2-konva';
import { ColorPickerModule } from 'ngx-color-picker';

import { LayoutComponent } from './components/layout/layout.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeroComponent } from './components/hero/hero.component';
import { LogoComponent } from './components/logo/logo.component';
import { ProgressComponent } from './components/progress/progress.component';
import { EditPanelComponent } from './components/edit-panel/edit-panel.component';
import { DeleteConfirmComponent } from './components/delete-confirm/delete-confirm.component';

@NgModule({
    declarations: [
        LayoutComponent,
        ToolbarComponent,
        FooterComponent,
        HeroComponent,
        LogoComponent,
        ProgressComponent,
        EditPanelComponent,
        DeleteConfirmComponent
    ],
    imports: [
        CommonModule,
        KonvaModule,
        RouterModule,
        ColorPickerModule,
        ReactiveFormsModule,
        FormsModule
    ],
    exports: [
        LayoutComponent,
        LogoComponent,
        KonvaModule,
        EditPanelComponent,
        ColorPickerModule,
        DeleteConfirmComponent
    ]
})
export class SharedModule {}
