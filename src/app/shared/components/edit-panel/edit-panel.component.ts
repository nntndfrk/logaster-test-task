import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FontsService } from 'src/app/core/services/fonts.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, config } from 'rxjs';
import { tap } from 'rxjs/operators';

import { SHAPES } from '../../../core/models/shape.model';
import { FontModel } from 'src/app/core/models/font.model';
import { LogoConfigModel } from 'src/app/core/models/logo-config.model';

@Component({
    selector: 'app-edit-panel',
    templateUrl: './edit-panel.component.html',
    styleUrls: ['./edit-panel.component.css']
})
export class EditPanelComponent implements OnInit {
    colors = {
        textColor: '#681b77',
        shapeColor: '#681b77'
    };

    editForm: FormGroup;

    cpWidth = 302;

    fonts: Observable<FontModel[]>;

    shapesList = SHAPES;

    @Input() config: LogoConfigModel;
    @Output() configChanged = new EventEmitter<LogoConfigModel>();

    constructor(private fontsService: FontsService, private fb: FormBuilder) {}

    ngOnInit() {
        this.buildForm();
        this.fonts = this.fontsService.getFonts();

        this.colors.shapeColor = this.config.shapeFillColor;
        this.colors.textColor = this.config.textColor;

        this.editForm.valueChanges.subscribe(formValues => {
            if (!this.editForm.invalid) {
                this.configChanged.next({
                    ...formValues,
                    shapeFillColor: this.colors.shapeColor,
                    textColor: this.colors.textColor
                });
            }
        });
    }

    buildForm() {
        this.editForm = this.fb.group({
            name: [this.config.name, [Validators.required]],
            slogan: [this.config.slogan, [Validators.required]],
            shape: [this.config.shape],
            fontFamily: [this.config.fontFamily]
        });
    }

    setColor() {
        if (!this.editForm.invalid) {
            this.configChanged.next({
                ...this.editForm.value,
                shapeFillColor: this.colors.shapeColor,
                textColor: this.colors.textColor
            });
        }
    }
}
