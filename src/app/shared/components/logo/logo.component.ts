import {
    Component,
    Input,
    OnInit,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { LogoConfigModel } from '../../../core/models/logo-config.model';

@Component({
    selector: 'app-logo',
    templateUrl: './logo.component.html',
    styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit, OnChanges {
    shape: string;
    defaultSize = 200;
    defaultFontSize = 36;
    gap = 12;
    @Input() config: LogoConfigModel;

    configStage = new BehaviorSubject<any>({});

    configCircle = new BehaviorSubject<any>({});

    configTriangle = new BehaviorSubject<any>({});

    configSquare = new BehaviorSubject<any>({});

    configText = new BehaviorSubject<any>({});

    constructor() {}

    ngOnInit() {
        this.shape = this.config.shape;
        this.configureIcon();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.config = changes.config.currentValue;
        this.shape = this.config.shape;
        this.configureIcon();
    }

    configureIcon() {
        this.configStage.next({
            width: this.config.size || this.defaultSize,
            height: this.config.size || this.defaultSize
        });

        if (this.shape === 'square') {
            this.configureSquare();
        } else if (this.shape === 'triangle') {
            this.configureTriangle();
        } else {
            this.configureCircle();
        }

        this.configureText();
    }

    configureCircle() {
        this.configCircle.next({
            x: this.calcSizeForShape(),
            y: this.calcSizeForShape() - this.defaultFontSize + this.gap,
            radius: this.calcSizeForShape() - this.defaultFontSize,
            fill: this.config.shapeFillColor
        });
    }

    configureTriangle() {
        this.configTriangle.next({
            x: this.calcSizeForShape(),
            y: this.calcSizeForShape() - this.defaultFontSize + 2 * this.gap,
            radius: this.calcSizeForShape() - this.defaultFontSize,
            sides: 3,
            fill: this.config.shapeFillColor
        });
    }

    configureSquare() {
        this.configSquare.next({
            x: this.calcSizeForShape(),
            y: this.calcSizeForShape() - this.defaultFontSize + this.gap,
            radius: this.calcSizeForShape() - this.defaultFontSize,
            sides: 4,
            fill: this.config.shapeFillColor,
            rotation: 45
        });
    }

    configureText() {
        document['fonts'].ready.then(fontFaceSet => {
            this.configText.next({
                x: 0,
                y: this.defaultSize - this.defaultFontSize,
                align: 'center',
                fontFamily: this.config.fontFamily,
                fontSize: this.defaultFontSize,
                text: this.config.slogan,
                fill: this.config.textColor,
                width: this.defaultSize
            });
        });
    }

    calcSizeForShape() {
        return this.config.size ? this.config.size / 2 : this.defaultSize / 2;
    }
}
