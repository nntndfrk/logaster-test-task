import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ProgressService } from 'src/app/core/services/progress.service';

@Component({
    selector: 'app-progress',
    templateUrl: './progress.component.html',
    styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {
    isShow: Observable<boolean>;

    constructor(private progressService: ProgressService) {}

    ngOnInit() {
        this.isShow = this.progressService.getLoadState();
    }
}
