import { Component } from '@angular/core';
import { DeleteConfirmService } from 'src/app/core/services/delete-confirm.service';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html',
  styleUrls: ['./delete-confirm.component.css']
})
export class DeleteConfirmComponent {

  constructor(public deleteConfirmService: DeleteConfirmService) { }

  cancel() {
    this.deleteConfirmService.pubData(false);
  }

  ok() {
    this.deleteConfirmService.pubData(true);
  }

}
